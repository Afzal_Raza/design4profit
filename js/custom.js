
new WOW().init();


$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:false,
    autoplay:true,
    autoplayTimeout:5000,
    animateOut: 'slideOutRight',
    animateIn: 'slideInRight',
    autoplayHoverPause:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
});


var form_name = $("form.briefing-form");
form_name.validate({
    errorElement: "div",
    errorClass: "error",
    // Specify validation rules
    rules: {
        name: {
            required: true
        },

        email: {
            required: true,
            email: true
        },
        phone: {
            required: true,
        },

        logo: {
            required: true,
        },
        tagline: {
            required: true,
        },
        type: {
            required: true,
        },

        detail: {
            required: true,
        },
    },
    errorPlacement: function(error,element) {
    return true;
    }

});

$("#file").on("change",function(){
    var pdfname = $("#file").val().replace(/C:\\fakepath\\/i, '');
    $(".file-name p").html(pdfname);
});

// $(".af-brief-form input").focus(function(){
//     $(this).addClass("focus");
// });

// $(".af-brief-form input").focusout(function(){
//     var input = $(this).val();
// });

// $(".af-brief-form textarea").focus(function(){
//     $(this).addClass("focus");
// });

$(".af-p").click(function(){
    $(".dropdown-menu.af-new-drop").css("display","block");
    $(".af-p").css("display","none");
    $(".af-m").css("display","block");
});

$(".af-m").click(function(){
    $(".dropdown-menu.af-new-drop").css("display","none");
    $(".af-p").css("display","block");
    $(".af-m").css("display","none");
});

(function($) {
  $.fn.visible = function(partial) {
    
      var $t            = $(this),
          $w            = $(window),
          viewTop       = $w.scrollTop(),
          viewBottom    = viewTop + $w.height(),
          _top          = $t.offset().top,
          _bottom       = _top + $t.height(),
          compareTop    = partial === true ? _bottom : _top,
          compareBottom = partial === true ? _top : _bottom;
    
    return ((compareBottom <= viewBottom) && (compareTop >= viewTop));

  };
    
})(jQuery);

var win = $(window);

var allMods = $(".image-grid__item");

allMods.each(function(i, el) {
  var el = $(el);
  if (el.visible(true)) {
    el.addClass("already-visible"); 
  } 
});

win.scroll(function(event) {
  
  allMods.each(function(i, el) {
    var el = $(el);
    if (el.visible(true)) {
      el.addClass("come-in"); 
    } 
  });
  
});

$(document).ready(function(){
  $(".load-box").addClass("animation");

});

$(".dropdown.login .dropdown-toggle").click(function(){
    $("body").addClass("background");
});

$(".full").click(function(){
    $("body").removeClass("background");
});

$(".af-login-box a").click(function(){
    $("body").removeClass("background");
});