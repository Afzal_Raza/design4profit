<?php include("header.php"); ?>
<?php include("navbar2.php"); ?>

<section class="faq-banner brief">
	<div class="container">
		<h1>Which Pricing package would you like?</h1>
	</div>
</section>

<section class="pricing-body">
	<div class="container">
		<div class="custom-dropdown">
			<label>Pricing for</label>
			<div class="af-select">
				<select>
					<option>Websites</option>
					<option></option>
					<option></option>
					<option></option>
				</select>
				<i class="fa fa-caret-down" aria-hidden="true"></i>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 col-md-6 pt-50">
				<div class="af-inner-box">
					<div class="laptop-img">
						<img src="images/laptop.png">
					</div>
					<div class="main-img">
						<img src="images/standardweb.png">
					</div>
					<div class="af-inner-footer">
						<h2>STANDARD WEBSITE</h2>
						<h3>(Mobile Responsive Design)</h3>
						<h4>From $1000</h3>
					</div>
				</div>
			</div>
			<div class="col-sm-6 col-md-6 pt-50">
				<div class="af-inner-box">
					<div class="laptop-img">
						<img src="images/laptop2.png">
					</div>
					<div class="main-img">
						<img src="images/header-imgs.png">
					</div>
					<div class="af-inner-footer">
						<h2>E-COMMERCE WEBSITE</h2>
						<h3>(Mobile Responsive Design)</h3>
						<h4>From $1500</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="faq-pricing">
	<div class="container">
		<div class="tab-content">
		    <div id="faq-logo" class="tab-pane fade in active">
		       <div class="faq-logo-tab">
		       		<h2>Which one should you choose ?</h2>
		       		<div class="panel-group" id="accordion">
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a class="accordion-toggle collapsed" href="javascript:void(0)">
					          <span>If you need a basic website with no shopping cart or checkout features, then a Standard Website will be perfect.</span>
					        </a>
					      </h4>
					    </div>
					  </div>
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a class="accordion-toggle collapsed"href="javascript:void(0)">
					          <span>If you’d like to sell products directly from your site, and you need a shopping cart, then choose our E-Commerce Package</span>
					        </a>
					      </h4>
					    </div>
					  </div>
					</div>
		       </div>
		    </div>
		</div>
	</div>
</section>

<?php include("foot.php") ?>
<?php include("footer.php") ?>