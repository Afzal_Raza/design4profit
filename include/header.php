<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Design4profit</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="OwlCarousel/dist/assets/owl.carousel.min.css">
<link rel="stylesheet" type="text/css" href="OwlCarousel/dist/assets/owl.theme.default.min.css">
<link rel="stylesheet" type="text/css" href="css/animate.css">
<link rel="stylesheet" href="css/infinite-scroll-docs.css" media="screen">
<link rel="stylesheet" type="text/css" href="css/rangeslider.css">
<link rel="icon" href="images/favi.png">
<link rel="stylesheet" type="text/css" href="http://yui.yahooapis.com/3.18.1/build/cssreset-context/cssreset-context-min.css">
<link href="css/style.css" rel="stylesheet">	
</head>
<body>