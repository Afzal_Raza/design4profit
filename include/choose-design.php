<?php include("header.php"); ?>
<?php include("navbar2.php"); ?>

<section class="faq-banner">
	<div class="container">
		<h1>What would you like designed?</h1>
	</div>
</section>
<section class="design-body">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-6">
				<a href="creative-briefing" class="af-design-box">
					<div class="price-tag">
						<img src="images/price-label.png">
						<p>From $299</p>
					</div>
					<div class="design-img">
						<img src="images/speller.png">
					</div>
					<div class="design-footer">
						<h2>Logo design</h2>
					</div>
				</a>
			</div>
			<div class="col-md-4 col-sm-6">
				<a href="javascript:void(0);" class="af-design-box">
					<div class="price-tag">
						<img src="images/price-label.png">
						<p>From $999</p>
					</div>
					<div class="design-img">
						<img src="images/trending.png">
					</div>
					<div class="design-footer">
						<h2>trending websites</h2>
					</div>
				</a>
			</div>
			<div class="col-md-4 col-sm-6 mbt-50">
				<a href="javascript:void(0);" class="af-design-box">
					<div class="price-tag">
						<img src="images/price-label.png">
						<p>From $99</p>
					</div>
					<div class="design-img">
						<img src="images/business-card.png">
					</div>
					<div class="design-footer">
						<h2>business cards</h2>
					</div>
				</a>
			</div>
			<div class="col-md-4 col-sm-6 mt-50">
				<a href="javascript:void(0);" class="af-design-box">
					<div class="price-tag">
						<img src="images/price-label.png">
						<p>From $199</p>
					</div>
					<div class="design-img">
						<img src="images/stationary.png">
					</div>
					<div class="design-footer">
						<h2>stationary</h2>
					</div>
				</a>
			</div>
			<div class="col-md-4 col-sm-6 mt-50">
				<a href="javascript:void(0);" class="af-design-box">
					<div class="price-tag">
						<img src="images/price-label.png">
						<p>From $299</p>
					</div>
					<div class="design-img">
						<img src="images/flyer.png">
					</div>
					<div class="design-footer">
						<h2>flyer</h2>
					</div>
				</a>
			</div>
			<div class="col-md-4 col-sm-6 mt-50">
				<a href="javascript:void(0);" class="af-design-box">
					<div class="price-tag">
						<img src="images/price-label.png">
						<p>From $299</p>
					</div>
					<div class="design-img">
						<img src="images/infographics.png">
					</div>
					<div class="design-footer">
						<h2>infographics</h2>
					</div>
				</a>
			</div>
			<div class="col-md-4 col-sm-6 mt-50">
				<a href="javascript:void(0);" class="af-design-box">
					<div class="price-tag">
						<img src="images/price-label.png">
						<p>From $200</p>
					</div>
					<div class="design-img">
						<img src="images/roller.png">
					</div>
					<div class="design-footer">
						<h2>Roller banners</h2>
					</div>
				</a>
			</div>
			<div class="col-md-4 col-sm-6 mt-50">
				<a href="javascript:void(0);" class="af-design-box">
					<div class="price-tag">
						<img src="images/price-label.png">
						<p>From $299</p>
					</div>
					<div class="design-img">
						<img src="images/card-design.png">
					</div>
					<div class="design-footer">
						<h2>Post card design</h2>
					</div>
				</a>
			</div>
			<div class="col-md-4 col-sm-6 mt-50">
				<a href="javascript:void(0);" class="af-design-box">
					<div class="price-tag">
						<img src="images/price-label.png">
						<p>From $299</p>
					</div>
					<div class="design-img">
						<img src="images/poster-design.png">
					</div>
					<div class="design-footer">
						<h2>poster design</h2>
					</div>
				</a>
			</div>
			<div class="col-md-4 col-sm-6 mt-50">
				<a href="javascript:void(0);" class="af-design-box">
					<div class="price-tag">
						<img src="images/price-label.png">
						<p>From $299</p>
					</div>
					<div class="design-img">
						<img src="images/product-design.png">
					</div>
					<div class="design-footer">
						<h2>product packaging</h2>
					</div>
				</a>
			</div>
			<div class="col-md-4 col-sm-6 mt-50">
				<a href="javascript:void(0);" class="af-design-box">
					<div class="price-tag">
						<img src="images/price-label.png">
						<p>From $320</p>
					</div>
					<div class="design-img">
						<img src="images/magazine.jpg">
					</div>
					<div class="design-footer">
						<h2>magazine</h2>
					</div>
				</a>
			</div>
			<div class="col-md-4 col-sm-6 mt-50 mb-100">
				<a href="javascript:void(0);" class="af-design-box">
					<div class="price-tag">
						<img src="images/price-label.png">
						<p>From $299</p>
					</div>
					<div class="design-img">
						<img src="images/email-temp.png">
					</div>
					<div class="design-footer">
						<h2>e-mail templates</h2>
					</div>
				</a>
			</div>
			<div class="goto-work-box">
				<a href="javascript:void(0)">get started</a>
			</div>
		</div>
	</div>
</section>


<?php include("foot.php") ?>
<?php include("footer.php") ?>