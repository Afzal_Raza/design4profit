<?php include("header.php"); ?>
<?php include("navbar2.php"); ?>

<section class="client-testimonials">
<div class="main">
  <div class="container container--wide">
    <div class="image-grid are-images-unloaded" data-js="image-grid">
      <div class="image-grid__col-sizer"></div>
      <div class="image-grid__gutter-sizer"></div>
          <div class="image-grid__item color-pink">
            <h6>We’ve been disappointed (screwed, actually) by other so-called design companies. But you did exactly what we agreed to, and you delivered early. Amazing service, great design. Thanks again.
            </h6>
            <p>Wanda S. </p>
            <p>Senior Director of Marketing,</p>
            <p>FDSI Enterprises</p>
          </div>
          <div class="image-grid__item color-sky">
            <h6>Wow! The business card you delivered looks like a collector’s item. Everyone i hand it to asks for another to show their boss. I have a feeling that’s the best recommendation ever.</h6>
            <p>Alphonso H.</p>
            <p>Media Mgr. Hilton Head Island </p>
          </div>
    </div>

    <div class="scroller-status">
      <div class="loader-ellips infinite-scroll-request">
        <span class="loader-ellips__dot"></span>
        <span class="loader-ellips__dot"></span>
        <span class="loader-ellips__dot"></span>
        <span class="loader-ellips__dot"></span>
      </div>
      <p class="scroller-status__message infinite-scroll-last"></p>
      <p class="scroller-status__message infinite-scroll-error"></p>
    </div>

      <p class="pagination">
        <a class="pagination__next" href="testimonial4"></a>
      </p>

    <footer class="full-page-demo-footer"></footer>

  </div> 
</div>
</section>


<?php include("foot.php") ?>
<?php include("footer.php") ?>