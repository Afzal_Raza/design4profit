
<section class="main-footer mt-5mi">
	<div class="container">
		<div class="footer-contents">
			<img src="images/footer-logo.svg">
			<ul>
				<li><a href="start-project">start your project</a></li>
				<li><a href="about-us">about us</a></li>
				<li><a href="javascript:void(0)">pricing</a></li>
				<li><a href="javascript:void(0)">gallery</a></li>
				<li><a href="faq">faq</a></li>
				<li><a href="testimonial">testimonial</a></li>
			</ul>
			<h4>Reach Us</h4>
			<div class="socials-icons">
				<a class="fa" href="javascript:void(0)"><i class="fa fa-facebook" aria-hidden="true"></i></a>
				<a class="twi" href="javascript:void(0)"><i class="fa fa-twitter" aria-hidden="true"></i></a>
				<a class="link" href="javascript:void(0)"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
				<a class="you" href="javascript:void(0)"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
			</div>
		</div>
	</div>
</section>
<section class="bottom-footer">
	<div class="container">
		<p>© 2019, All Rights Reserved</p>
	</div>
</section>