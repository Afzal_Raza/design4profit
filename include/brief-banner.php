<?php include("header.php"); ?>
<?php include("navbar2.php"); ?>

<section class="faq-banner business-card">
	<div class="container">
		<h1>Fill out your creative brief to get started?</h1>
	</div>
</section>

<section class="business-card-body">
	<div class="container">
		<div class="row">
			<div class="col-xs-6 col-sm-3 col-md-3">
				<h2>1: CHOOSE PROJECT</h2>
				<div class="load-box redfull">
					<p>100 %</p>
				</div>
			</div>
			<div class="col-xs-6 col-sm-3 col-md-3">
				<h2>2: CREATIVE BRIEF</h2>
				<div class="load-box redfull">
					<p>100 %</p>
				</div>
			</div>
			<div class="col-xs-6 col-sm-3 col-md-3">
				<h2>3: CHOOSE PACKAGE</h2>
				<div class="load-box redfull">
					<p>100 %</p>
				</div>
			</div>
			<div class="col-xs-6 col-sm-3 col-md-3">
				<h2>4: CHECKOUT</h2>
				<div class="load-box redfull">
					<p>100 %</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-7 col-md-7">
				<div class="business-filling">
					<h3>Start your Banner Ad design project by filling out your creative brief below</h3>
					<h4>Need help filling out your creative brief ? Give us a call at 747.666. LOGO (5646) anytime M-F, from 9AM to 8PM US Eastern.</h4>
					<a href="javascript:void(0)">Click here to chat!</a>
					<div class="card-mockup">
						<img src="images/businnes-img.png">
					</div>
				</div>
				<div class="business-form-box">
					<form method="post" action="">
						<h3>background <span>information</span></h3>
						<div class="af-cards-input">
							<label>What is the name of your business or organization?</label>
							<input type="text" name="">
						</div>
						<div class="af-textareas pb-20">
							<label>Tell us about your business - What do you do? Who are your customers?</label>
							<textarea></textarea>
						</div>
						<div class="col-md-12 plr-5 pob-30">
							<div class="row">
								<div class="col-md-7">
									<h4>What industry is your business in?</h4>
								</div>
								<div class="col-md-5">
									<select>
										<option>- Select Industry -</option>
										<option>IT Industry</option>
										<option>Medical Industry</option>
										<option>Advertising Industry</option>
										<option>Insurance Industry</option>
										<option>Media Industry</option>
									</select>
								</div>
							</div>
						</div>
						
						<div class="af-cards-input">
							<label>How many employees do you need cards for?</label>
							<input type="text" name="">
						</div>
						<div class="af-range-slider">
							<div class="range-box">
								<label class="title">What emotions do you want your ads to convey?</label>
								<div class="row">
									<div class="col-xs-4 col-sm-4 col-md-2">
										<label>Luxury</label>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4">
										<input class="slider" type="range" min="0" max="100" step="25">
									</div>
									<div class="col-xs-4 col-sm-4 col-md-2 af-right">
										<label>Value</label>
									</div>	
								</div>
							</div>
							<div class="range-box">
								<div class="row">
									<div class="col-xs-4 col-sm-4 col-md-2">
										<label>Masculine</label>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4">
										<input class="slider" type="range" min="0" max="100" step="25">
									</div>
									<div class="col-xs-4 col-sm-4 col-md-2  af-right">
										<label>Feminine</label>
									</div>	
								</div>
							</div>
							<div class="range-box">
								<div class="row">
									<div class="col-xs-4 col-sm-4 col-md-2">
										<label>Traditional</label>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4 ">
										<input class="slider" type="range" min="0" max="100" step="25">
									</div>
									<div class="col-xs-4 col-sm-4 col-md-2 af-right">
										<label>Modern</label>
									</div>	
								</div>
							</div>
							<div class="range-box">
								<div class="row">
									<div class="col-xs-4 col-sm-4 col-md-2">
										<label>Serious</label>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4">
										<input class="slider" type="range" min="0" max="100" step="25">
									</div>
									<div class="col-xs-4 col-sm-4 col-md-2 af-right">
										<label>Fun</label>
									</div>	
								</div>
							</div>
							<div class="range-box">
								<div class="row">
									<div class="col-xs-4 col-sm-4 col-md-2">
										<label>Safe</label>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4">
										<input class="slider" type="range" min="0" max="100" step="25">
									</div>
									<div class="col-xs-4 col-sm-4 col-md-2 af-right">
										<label>Daring</label>
									</div>	
								</div>
							</div>
							<div class="range-box">
								<div class="row">
									<div class="col-xs-4 col-sm-4 col-md-2">
										<label>Simple</label>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4">
										<input class="slider" type="range" min="0" max="100" step="25">
									</div>
									<div class="col-xs-4 col-sm-4 col-md-2 af-right">
										<label>Detailed</label>
									</div>	
								</div>
							</div>
						</div>
						<div class="col-md-12 plr-0 chck-label">
							<h3 class="pb-30">How do you plan to use your logo?</h3>
							<label>
								<input type="checkbox" name="">
								The "Google Pack" -- most common dimensions for Google Ad Network
							</label>
							<label>
								<input type="checkbox" name="">
								The "Bing Pack" -- most common dimensions for Microsoft Bing Ad Network
							</label>
							<label>
								<input type="checkbox" name="">
								120 x 600
							</label>
							<label>
								<input type="checkbox" name="">
								160 x 600	
							</label>
							<label>
								<input type="checkbox" name="">
								300 x 250
							</label>
							<label>
								<input type="checkbox" name="">
								336 x 280
							</label>
							<label>
								<input type="checkbox" name="">
								468 x 60
							</label>
							<label>
								<input type="checkbox" name="">
								728 x 90
							</label>
							<label>
								<input type="checkbox" name="">
								I don't know
							</label>
							<label>
								<input type="checkbox" name="">
								<input type="text" name="" placeholder="Something Else">
							</label>
						</div>
						<div class="af-textareas">
							<label>What specific text would you like in your ads?</label>
							<textarea placeholder="Do you have any specific instructions for our designers or Is there anything else you'd like to add?"></textarea>
						</div>
						<div class="af-textareas">
							<label>Anything else you'd like to tell us?</label>
							<textarea placeholder="Do you have any specific instructions for our designers or Is there anything else you'd like to add?"></textarea>
						</div>
						<div class="af-card-file">
							<div class="row">
								<div class="col-md-7">
									<p>If you have a logo you'd like included on the card, or if there are other files that would help our designers, please upload it here.</p>
								</div>
								<div class="col-md-5">
									<div class="af-choose-card">
										<input type="file" name="">
										Choose a File
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 text-right">
								<div class="af-subm-btn pot-40">
									<input type="submit" name="" value="NEXT">
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="col-sm-5 col-md-5">
				<div class="card-mockup">
					<img src="images/businnes-img.png">
				</div>
			</div>
		</div>
	</div>
</section>


<?php include("foot.php") ?>
<?php include("footer.php") ?>