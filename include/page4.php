<?php include("header.php"); ?>
<?php include("navbar2.php"); ?>

<section class="client-testimonials">
<div class="main">
  <div class="container container--wide">
    <div class="image-grid are-images-unloaded" data-js="image-grid">
      <div class="image-grid__col-sizer"></div>
      <div class="image-grid__gutter-sizer"></div>
         <div class="image-grid__item color-slategrey">
          <h6>Our business jumped significantly when we promoted our new logo. So glad we did this.</h6>
          <p>Mike Jarvis</p>
          <p>American Steakhouse Restaurants</p>
          <p>St. Louis, MO</p>
        </div>
        <div class="image-grid__item color-antique">
          <h6>Efficient, fast, friendly. With a great product. Can’t ask for more. Thanks.</h6>
          <p>Marsha Dorner</p>
          <p>Western Fashion Center</p>
          <p> Los Angeles, Ca</p>
        </div>

          <div class="image-grid__item color-purple">
              <h6>You guys are the real deal. No redundant “clip art” here. My new business logo has already brought new business.</h6>
              <p>Dave M., CMO Allied </p>
              <p>Enterprises, Los Angeles, Ca.</p>
          </div>
          <div class="image-grid__item color-blue">
             <h6>We’ve used the same website design for almost ten years and our revenue growth was stalled. Your design now has us at the top of the search engines, and new customers are streaming in. I’m just glad we found you when we did.</h6>
              <p>H. Steinbaum  </p>
              <p>President</p>
              <p>WhereHollywoodHides.com</p>
          </div>
          <div class="image-grid__item color-skin">
            <h6>It’s hard to believe that a new logo could increase our quarterly revenues, but there is no other explanation. You did exactly what you said you would...you brought more customers to our site!</h6>
            <p>Hillary M. </p>
            <p>CFO surfshirts.com</p>
          </div>
          <div class="image-grid__item color-green">
            <h6>Your design team was very helpful and easy to work with...even though we wanted lots of adjustments to our printed sales materials. The final product made our competitors envious.</h6>
            <p>Sean O.</p>
            <p>Field Sales Mgr.</p>
            <p>The Bardash Company</p>
          </div>
          
    </div>

    <div class="scroller-status">
      <div class="loader-ellips infinite-scroll-request">
        <span class="loader-ellips__dot"></span>
        <span class="loader-ellips__dot"></span>
        <span class="loader-ellips__dot"></span>
        <span class="loader-ellips__dot"></span>
      </div>
      <p class="scroller-status__message infinite-scroll-last"></p>
      <p class="scroller-status__message infinite-scroll-error"></p>
    </div>

      <p class="pagination">
        <a class="pagination__next" href="testimonial5"></a>
      </p>

    <footer class="full-page-demo-footer"></footer>

  </div> 
</div>
</section> 


<?php include("foot.php") ?>
<?php include("footer.php") ?>