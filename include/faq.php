<?php include("header.php"); ?>
<?php include("navbar2.php"); ?>

<section class="faq-banner">
	<div class="container">
		<h1>FREQUENTLY ASKED QUESTIONS</h1>
	</div>
</section>
<section class="faq-tabs">
	<div class="container">
	  <ul class="nav nav-tabs">
	    <li class="active">
	    	<a data-toggle="tab" href="#faq-logo">
	    		<div class="af-img-boxe">
	    			<img src="images/faq-1-icon.png">
	    		</div>
	    		<h4>Logo & Design</h4>
	    	</a>
	    </li>
	    <li>
	    	<a data-toggle="tab" href="#faq-web">
	    		<div class="af-img-boxe">
	    			<img src="images/faq-2-icon.png">
	    		</div>
	    		<h4>Web Design</h4>
	    	</a>
	    </li>
	    <li>
	    	<a data-toggle="tab" href="#faq-pricing">
	    		<div class="af-img-boxe">
	    			<img src="images/faq-3-icon.png">
	    		</div>
	    		<h4>Pricing & Payment</h4>
	    	</a>
	    </li>
	    <li>
	    	<a data-toggle="tab" href="#faq-gurrante">
	    		<div class="af-img-boxe">
	    			<img src="images/faq-4-icon.png">
	    		</div>
	    		<h4>Satisfaction Guarantee</h4>
	    	</a>
	    </li>
	  </ul>

	  <div class="tab-content">
	    <div id="faq-logo" class="tab-pane fade in active">
	       <div class="faq-logo-tab">
	       		<h2>Logos & Design</h2>
	       		<div class="panel-group" id="accordion">
				  <div class="panel panel-default">
				    <div class="panel-heading">
				      <h4 class="panel-title">
				        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
				          <span>How long does it take me to get my order delivered?</span>
				        </a>
				      </h4>
				    </div>
				    <div id="collapseOne" class="panel-collapse collapse">
				      <div class="panel-body">
				        <p>It really depends upon the nature of your order.</p>
						<p>We can deliver initial iterations of a logo design concept within 72 hours. A final draft of your customized logo design will take another 48 hours.</p>
						<p>Once your logo designed is finalized and approved, print items (business cards, posters, etc) can be delivered to you within days.</p>
						<p>Web design, company branding, business consultation based upon your design are all on an individual basis.</p>
						<p>Rarely do we take longer than 7 days to complete any project, because we have an entire team of design specialists in house and ready to jump on your project.</p>

				      </div>
				    </div>
				  </div>
				  <div class="panel panel-default">
				    <div class="panel-heading">
				      <h4 class="panel-title">
				        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
				          <span>How do you deliver my design project to me?</span>
				        </a>
				      </h4>
				    </div>
				    <div id="collapseTwo" class="panel-collapse collapse">
				      <div class="panel-body">
				        <p>We want you to have complete access and flexibility with the design projects we produced for you.</p>
				         <p>You may specify any file format you want, and we can deliver your work in multiple formats, including JPG, TIF, PDF, PNG, EPS, etc.</p>
				          <p>Whatever you need, that’s what we deliver!</p>
				      </div>
				    </div>
				  </div>
				  <div class="panel panel-default">
				    <div class="panel-heading">
				      <h4 class="panel-title">
				        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
				          <span>What if I need revisions?</span>
				        </a>
				      </h4>
				    </div>
				    <div id="collapseThree" class="panel-collapse collapse">
				      <div class="panel-body">
				       <p>At the beginning of your project, we’ll offer you several design concept choices. Once you respond and we get a better idea of what appeals to you, we’ll revise those choices and whittle them down a bit. You’ll always have a voice in the colors, tone, theme, and market direction of any design work we perform for you. Professional design with an eye toward increasing your business activity requires a bit of collaboration...and a lot of work on our part.  The better our flow of communication, the more specific your feedback, the better the finished product. Revisions are where we “live,” so we work with you until we get it right!</p>
				      </div>
				    </div>
				  </div>
				  <div class="panel panel-default">
				    <div class="panel-heading">
				      <h4 class="panel-title">
				        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapsefour">
				         	<span>Will my design be copyrighted? Who owns it?</span>
				        </a>
				      </h4>
				    </div>
				    <div id="collapsefour" class="panel-collapse collapse">
				      <div class="panel-body">
				       <p>You are the “author” of the design. As soon as it’s produced, you own all copyrights exclusively.</p>
				      </div>
				    </div>
				  </div>
				  <div class="panel panel-default">
				    <div class="panel-heading">
				      <h4 class="panel-title">
				        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapsefive">
				         	<span>How do you assign my job to a designer? Is it a bid or “contest” process?</span>
				        </a>
				      </h4>
				    </div>
				    <div id="collapsefive" class="panel-collapse collapse">
				      <div class="panel-body">
				       <p>No. Let us repeat that: NO. We are an in-house team of professionals, well-versed and educated in design, branding, and business principles. We do not “farm out” any work or use random freelancers who bid jobs at the lowest price. Your project will be in the hands of responsible team leaders and design professionals at all times.</p>
				      </div>
				    </div>
				  </div>
				  <div class="panel panel-default">
				    <div class="panel-heading">
				      <h4 class="panel-title">
				        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapsesix">
				         <span>Will someone be specifically in charge of my project?</span>
				        </a>
				      </h4>
				    </div>
				    <div id="collapsesix" class="panel-collapse collapse">
				      <div class="panel-body">
				       <p>Yes. Regardless of your project size—whether it’s a simple logo or a more complex website/business branding task—your job is overseen by a designated Project Manager. This is the person with whom you will personally interface and full telephone, email, and texting contact throughout the life of your project. Your Project Manager is the one who will guide your project from start to finish and who will personally respond to all your concerns.</p>
				      </div>
				    </div>
				  </div>
				  <div class="panel panel-default">
				    <div class="panel-heading">
				      <h4 class="panel-title">
				        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseseven">
				        <span>Do your designers do the work on a “spec” or “bid” basis?</span>
				        </a>
				      </h4>
				    </div>
				    <div id="collapseseven" class="panel-collapse collapse">
				      <div class="panel-body">
				       <p>Again, the answer is NO. “Spec” work is, in our view completely unethical and exploitative of the skills of freelancers who will work for less than a living wage. Those so-called “designers” are almost always off-shore, are not well-trained, and rarely deliver acceptable quality work. Anyone touching your project here at Designs4Profit.com is in-house at our facility throughout the working day.</p>
				      </div>
				    </div>
				  </div>
				  <div class="panel panel-default">
				    <div class="panel-heading">
				      <h4 class="panel-title">
				        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseeight">
				        	<span>Where are your offices?</span>
				        </a>
				      </h4>
				    </div>
				    <div id="collapseeight" class="panel-collapse collapse">
				      <div class="panel-body">
				       <p>We are located in Fremont, California. Directly across San Francisco Bay from Palo Alto. We draw our designer talent from nearby Silicon Valley and from the arts community in San Francisco. With a design talent pool like that, we’re guaranteed the best team on the planet.</p>
				      </div>
				    </div>
				  </div>
				  <div class="panel panel-default">
				    <div class="panel-heading">
				      <h4 class="panel-title">
				        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapsenine">
				        	<span>Wouldn’t it be cheaper to use designers from overseas?</span>
				        </a>
				      </h4>
				    </div>
				    <div id="collapsenine" class="panel-collapse collapse">
				      <div class="panel-body">
				       <p>Of course it would. But the work would not compare to what our team produces. We recruit from the best West Coast designers, give them a thorough “audition” period, and when they demonstrate the appropriate skill level and work ethics, we pay a very competitive wage with production incentives and bonuses to everyone on the team. (It’s a great place to work; if you’re looking for design excellence and have the skills, contact us!)</p>
				      </div>
				    </div>
				  </div>
				</div>
	       </div>
	    </div>
	    <div id="faq-web" class="tab-pane fade">
	      	<div class="faq-logo-tab">
	       		<h2>Web Design</h2>
	       		<div class="panel-group" id="accordion">
				  <div class="panel panel-default">
				    <div class="panel-heading">
				      <h4 class="panel-title">
				        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseten">
				         	<span>If I order a full website design & function package, do you provide the necessary coding and back-end work?</span>
				        </a>
				      </h4>
				    </div>
				    <div id="collapseten" class="panel-collapse collapse">
				      <div class="panel-body">
				        <p>Yes. We can provide total front-to-back website services, including coding, special payment systems, data collection, and server integration.</p>
				      </div>
				    </div>
				  </div>
				  <div class="panel panel-default">
				    <div class="panel-heading">
				      <h4 class="panel-title">
				        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseeleven">
				          	<span>What if I need hosting for my new website?</span>
				        </a>
				      </h4>
				    </div>
				    <div id="collapseeleven" class="panel-collapse collapse">
				      <div class="panel-body">
				        <p>We don’t provide hosting, but we have amazing relationships with the best hosting services in the U.S., where the security of your site data is assured.  Let us know and we’ll make the introductions so that you can do it all in a phone call.</p>
				      </div>
				    </div>
				  </div>
				  <div class="panel panel-default">
				    <div class="panel-heading">
				      <h4 class="panel-title">
				        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapsetwelve">
				          	<span>What kind of content management systems do you use? Will I be able to “work” my own website when you’re done with my project?</span>
				        </a>
				      </h4>
				    </div>
				    <div id="collapsetwelve" class="panel-collapse collapse">
				      <div class="panel-body">
				       <p>We’re system agnostic...which means we can work with whatever system you like best. Wordpress, php, Weebly, Wiix, Squarespace...what works best for you works for us.</p>
				      </div>
				    </div>
				  </div>
				</div>
	       </div>
	    </div>
	    <div id="faq-pricing" class="tab-pane fade">
	      	<div class="faq-logo-tab">
	       		<h2>Pricing & Payment</h2>
	       		<div class="panel-group" id="accordion">
				  <div class="panel panel-default">
				    <div class="panel-heading">
				      <h4 class="panel-title">
				        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapsethirteen">
				        	<span>How does payment work? Do you require a deposit or...?</span>
				        </a>
				      </h4>
				    </div>
				    <div id="collapsethirteen" class="panel-collapse collapse">
				      <div class="panel-body">
				        <p>As soon as you describe your project and business objectives, you’ll go through a straightforward and secure checkout process. We do require 50% deposit on all orders, which means that our team goes to work for you immediately. The balance of your fee is due upon delivery, and when you are 100% thrilled with our work.</p>
				      </div>
				    </div>
				  </div>
				  <div class="panel panel-default">
				    <div class="panel-heading">
				      <h4 class="panel-title">
				        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapsefourteen">
				          	<span>Do you accept Paypal?</span>
				        </a>
				      </h4>
				    </div>
				    <div id="collapsefourteen" class="panel-collapse collapse">
				      <div class="panel-body">
				        <p>Sure. We also accept all major credit cards and bank transfers. Your choice is our choice!</p>
				      </div>
				    </div>
				  </div>
				  <div class="panel panel-default">
				    <div class="panel-heading">
				      <h4 class="panel-title">
				        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapsefifteen">
				          	<span>Do you accept payment in bitcoin or foreign funds?</span>
				        </a>
				      </h4>
				    </div>
				    <div id="collapsefifteen" class="panel-collapse collapse">
				      <div class="panel-body">
				       <p>Sorry, but we work only with U.S. currency. All payments are in dollars.</p>
				      </div>
				    </div>
				  </div>
				</div>
	       </div>
	    </div>
	    <div id="faq-gurrante" class="tab-pane fade">
	      	<div class="faq-logo-tab">
	       		<h2>Satisfaction Guarantee</h2>
	       		<div class="panel-group" id="accordion">
				  <div class="panel panel-default">
				    <div class="panel-heading">
				      <h4 class="panel-title">
				        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapsesixteen">
				        	<span>Do you guarantee the work I order?</span>
				        </a>
				      </h4>
				    </div>
				    <div id="collapsesixteen" class="panel-collapse collapse">
				      <div class="panel-body">
				        <p>Absolutely. You must be 100% satisfied with everything we do or we’ll revise everything until we exceed your expectations. This guarantee covers all the design work we offer, from Logo Design to Website Design & Construction. There are no exclusions to our guarantee of design services.</p>
				      </div>
				    </div>
				  </div>
				  <div class="panel panel-default">
				    <div class="panel-heading">
				      <h4 class="panel-title">
				        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseseventeen">
				       	<span> My business is confidential and proprietary. How do I know my privacy will be protected?</span>
				        </a>
				      </h4>
				    </div>
				    <div id="collapseseventeen" class="panel-collapse collapse">
				      <div class="panel-body">
				        <p>We’ve been in business long enough to understand that everything transpiring between our clients and ourselves is “protected” information. None of your data, business operations or objectives, or your design work is ever shared with third parties, including domestic or foreign business interests or governments.</p>
				      </div>
				    </div>
				  </div>
				  <div class="panel panel-default">
				    <div class="panel-heading">
				      <h4 class="panel-title">
				        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseeighteen">
				         	<span>What's your privacy policy?</span>
				        </a>
				      </h4>
				    </div>
				    <div id="collapseeighteen" class="panel-collapse collapse">
				      <div class="panel-body">
				       <p>Simply put, we don’t share or sell your information to a third party without your explicit permission. In other words, we wouldn’t do anything that you wouldn’t do.  Read our full privacy policy here.</p>
				      </div>
				    </div>
				  </div>
				</div>
	       </div>
	    </div>
	  </div>
	</div>
</section>


<?php include("foot.php") ?>
<?php include("footer.php") ?>