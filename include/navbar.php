<div class="full"></div>
<section class="header-sec">
	<div class="container">
    	<div class="row">
        	<nav class="navbar navbardefault">
			  <div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<i class="fa fa-bars" aria-hidden="true"></i>
				</button>
				<a class="navbar-brand" href="./"><img class="logo-img" src="images/site-logo.svg"></a>
			  </div>
			  <div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-right ">
					<li class="dropdown mob">
						<div class="mobile-icons">
							<i class="fa fa-plus af-p" aria-hidden="true"></i>
							<i class="fa fa-minus af-m" aria-hidden="true"></i>
						</div>
						<a class="dropdown-toggle" href="start-project">Start Your Project</a>
						<ul class="dropdown-menu af-new-drop">
							<li>
								<a href="logo-pricing">
									<div class="left-cont">
										<h5>Logo Design</h5>
										<h6>from $300</h6>
									</div>
									<div class="right-img">
										<img src="images/logo-design.png">
									</div>
								</a>
							</li>
							<li>
								<a href="pricing-website">
									<div class="left-cont">
										<h5>websites</h5>
										<h6>from $300</h6>
									</div>
									<div class="right-img">
										<img src="images/webnav.jpg">
									</div>
								</a>
							</li>
							<li>
								<a href="business-card">
									<div class="left-cont">
										<h5>b.cards</h5>
										<h6>from $300</h6>
									</div>
									<div class="right-img">
										<img src="images/bcardnav.jpg">
									</div>
								</a>
							</li>
							<li>
								<a href="">
									<div class="left-cont">
										<h5>stationery</h5>
										<h6>from $300</h6>
									</div>
									<div class="right-img">
										<img src="images/stationery.jpg">
									</div>
								</a>
							</li>
							<li>
								<a href="">
									<div class="left-cont">
										<h5>poster</h5>
										<h6>from $300</h6>
									</div>
									<div class="right-img">
										<img src="images/poster.jpg">
									</div>
								</a>
							</li>
							<li>
								<a href="">
									<div class="left-cont">
										<h5>mobile app</h5>
										<h6>from $300</h6>
									</div>
									<div class="right-img">
										<img src="images/mobile-app.jpg">
									</div>
								</a>
							</li>
						</ul>
					</li>
					<li class=""><a href="about-us">About us</a></li>
					<li class=""><a href="javascript:void(0);">Pricing</a></li>
					<li class=""><a href="javascript:void(0);">Gallery</a></li>
					<li class=""><a href="faq">FAQ</a></li>
					<li class="dropdown login">
						<a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" href="javascript:void(0);">Login</a>
						<ul class="dropdown-menu">
	                        <li>
                        		<div class="af-login-box">
	                        		<form method="post" action="" autocomplete="off">
	                        			<div class="af-input-boxes">
		                        			<input type="text" name="username" placeholder="Username/Email">
		                        		</div>
		                        		<div class="af-input-boxes">
		                        			<input type="password" name="password" placeholder="Password">
		                        		</div>
		                        		<div class="af-input-boxes">
		                        			<input type="submit" value="SIGN IN">
		                        		</div>
	                        		</form>
	                        		<div class="col-xs-6">
	                        			<label>
	                        				<input type="checkbox" name="remember"> Remember Me
	                        			</label>
	                        		</div>
	                        		<div class="col-xs-6">
	                        			<a class="af-forget" href="javascript:void(0)">Forget Password?</a>
	                        		</div>
	                        		<ul class="login-with">
	                        			<li><a href="javascript:void(0)"><i class="fa fa-google" aria-hidden="true"></i> Login with Google</a></li>
	                        			<li><a href="javascript:void(0)"><i class="fa fa-facebook" aria-hidden="true"></i> Login with Facebook</a></li>
	                        			<li><a href="javascript:void(0)"><i class="fa fa-user" aria-hidden="true"></i> Register an Account</a></li>
	                        		</ul>
	                        	</div>
	                        </li>
	                    </ul>
					</li>
					<li class="af-desktop"><a href="javascript:void(0);"><i class="fa fa-search" aria-hidden="true"></i></a></li>  
				</ul> 
			  </div>
			  <div class="mobile-search"><a href="javascript:void(0);"><i class="fa fa-search" aria-hidden="true"></i></a></div>
          	</nav>
        </div>
    </div>
</section>