<?php include("header.php"); ?>
<?php include("navbar2.php"); ?>

<section class="client-testimonials">
<div class="main">
  <div class="container container--wide">
    <div class="image-grid are-images-unloaded" data-js="image-grid">
      <div class="image-grid__col-sizer"></div>
      <div class="image-grid__gutter-sizer"></div>

          <div class="image-grid__item color-hotpink">
            <h6>We ordered the Premier Design Package. It was top-to-bottom awesome.</h6>
            <p>Ronnie Gardner</p>
            <p>National Hot Rod Association</p>
            <p>Chicago, IL</p>
        </div>
        <div class="image-grid__item color-indigo">
          <h6>They weren’t the cheapest we could find, but after two horrible experiences, it was a relief to deal with real professionals.</h6>
          <p>Dora Ryan</p>
          <p>International Marketplace</p>
          <p>Seattle, WA</p>
        </div>
        <div class="image-grid__item color-mistyrose">
           <h6>Enjoyable process, great people, wonderful work. Highly recommended.</h6>
           <p>Tonya Lasser</p>
           <p>Income Tax Associates</p>
           <p>Philadelphia, PA</p>
        </div>
        <div class="image-grid__item color-gold">
            <h6>Designs4Profits.com made our business look exciting. Increased sales by 15% in the first month. Whoa.</h6>
            <p>Nathan Coombs</p>
            <p>Charleston Automotive</p>
            <p> Charleston, VA</p>
        </div>
        <div class="image-grid__item color-slate">
          <h6>Our personal design project manager handled it all. Could not have been easier and the result was more than we hoped for.</h6>
          <p>Wayne Miller</p>
          <p>San Diego Surf Company</p>
        </div>
        <div class="image-grid__item color-seagreen">
          <h6>Everybody we talked to was responsive and friendly. The designs required several revisions, but the end result was worth it.</h6>
          <p>Eric Timpanis</p>
          <p>Marketing Director</p>
          <p>Global Travel Services</p>
        </div>
        
    </div>

    <div class="scroller-status">
      <div class="loader-ellips infinite-scroll-request">
        <span class="loader-ellips__dot"></span>
        <span class="loader-ellips__dot"></span>
        <span class="loader-ellips__dot"></span>
        <span class="loader-ellips__dot"></span>
      </div>
      <p class="scroller-status__message infinite-scroll-last"></p>
      <p class="scroller-status__message infinite-scroll-error"></p>
    </div>

      <p class="pagination">
        <a class="pagination__next" href="testimonial4"></a>
      </p>

    <footer class="full-page-demo-footer"></footer>

  </div> 
</div>
</section> 


<?php include("foot.php") ?>
<?php include("footer.php") ?>