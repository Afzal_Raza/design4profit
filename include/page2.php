<?php include("header.php"); ?>
<?php include("navbar2.php"); ?>

<section class="header-sec blue-background">
  <div class="container">
      <div class="row">
          <nav class="navbar navbardefault">
        <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <i class="fa fa-bars" aria-hidden="true"></i>
        </button>
        <a class="navbar-brand" href="index.html"><img class="logo-img" src="images/site-logo.png"></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar-right ">
          <li class=""><a href="index.html">Start Your Project</a></li>
          <li class=""><a href="about-us.html">About us</a></li>
          <li class=""><a href="javascript:void(0);">Pricing</a></li>
          <li class=""><a href="javascript:void(0);">Gallery</a></li>
          <li class=""><a href="faq.html">FAQ</a></li>
          <li class=""><a href="javascript:void(0);">Login</a></li>
          <li class="af-desktop"><a href="javascript:void(0);"><i class="fa fa-search" aria-hidden="true"></i></a></li>  
        </ul> 
        </div>
        <div class="mobile-search"><a href="javascript:void(0);"><i class="fa fa-search" aria-hidden="true"></i></a></div>
            </nav>
        </div>
    </div>
</section>
<section class="client-testimonials">
<div class="main">
  <div class="container container--wide">
    <div class="col-md-12 col-lg-10 col-lg-offset-1">
    <div class="image-grid are-images-unloaded" data-js="image-grid">
      <div class="image-grid__col-sizer"></div>
      <div class="image-grid__gutter-sizer"></div>
      	   <div class="image-grid__item color-magenta">
	          <h6>Our founder created our first logo. Not good. Convinced him to try Designs4Profit.com. Now we look like professionals.</h6>
	          <p>George Goldenberg</p>
	          <p>Texas Oil & Exploration</p>
	          <p>Dallas Tx</p>
	        </div>
	        <div class="image-grid__item color-deepsky">
	          <h6>We needed a working website fast. We got it...and it sells our stuff like crazy. Thanks to you guys for everything!</h6>
	          <p>Tiffany Shoemaker</p>
	          <p>Tiffany.com</p>
	        </div>
	        <div class="image-grid__item color-wheat">
	          <h6>The stationery business can be pretty boring. Designs4Profit changed that and our business increased by 20% the first month.</h6>
	          <p>Margo Freeman</p>
	          <p>The Paper Mill</p>
	          <p>San Francisco, Ca.</p>
	        </div>	
          <div class="image-grid__item color-seashell">
            <h6>The company marketing materials we were using were stale. Thanks for the refresh.</h6>
            <p>John Quincy</p>
            <p>Metals Management Systems</p>
            <p>Phoenix, AZ</p>
          </div>
          <div class="image-grid__item color-mint">
            <h6>They were the high bid, but we liked what we saw. At the end of the day, their high bid was a bargain.</h6>
            <p>Monty Monan</p>
            <p>Florida Marlin Association</p>
            <p>Miami, FL</p>
        </div>
        
        <div class="image-grid__item color-dew">
            <h6>We’re in the customer service business too, but the team at Design4Profits.com showed us how it’s done properly. Very impressive folks. Excellent designs too!</h6>
            <p>Linda Laisa</p>
            <p>Saks Fifth Avenue, NYC</p>
        </div>
        
    </div>

    <div class="scroller-status">
      <div class="loader-ellips infinite-scroll-request">
        <span class="loader-ellips__dot"></span>
        <span class="loader-ellips__dot"></span>
        <span class="loader-ellips__dot"></span>
        <span class="loader-ellips__dot"></span>
      </div>
      <p class="scroller-status__message infinite-scroll-last"></p>
      <p class="scroller-status__message infinite-scroll-error"></p>
    </div>

      <p class="pagination">
        <a class="pagination__next" href="testimonial3"></a>
      </p>

    <footer class="full-page-demo-footer"></footer>
  </div>
  </div> 
</div> 
</section>



<?php include("foot.php") ?>
<?php include("footer.php") ?>