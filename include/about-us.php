<?php include("header.php"); ?>
<?php include("navbar2.php"); ?>

<section class="about-banner">
	<div class="container">
		<h1>MEET THE TEAM</h1>
	</div>
</section>
<section class="about-mission-sec">
	<div class="container-fluid">
		<div class="row min-hei">
			<div class="col-md-6 pl-custom pr-custom">
				<div class="mission-box">
					<h2>our <span>mission</span></h2>
					<p>The mission of every member of the team is to deliver designs that are more than “cool,” but that actually provide bottom-line value to you and your business.</p>
					<p>Unlike the majority of online “design providers,” every one of our design specialists is a full-time paid employee, working in-house.  We don’t simply subcontract assignments out to a database of unknown freelancers at <span>Upwork, Freelancer, or Remote.</span>com and offer them “the chance to compete for the gig.” That guarantees mediocre work product, exploitation of unqualified workers, and provides the customer with poor value for the money.</p>
				</div>
			</div>
			<div class="col-md-6">
				<div class="mission-img-box1">
					<img src="images/mission-img.png">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="different-img-box">
					<img src="images/different-img.png">
				</div>
			</div>
			<div class="col-md-6 pl-11 pr-11">
				<div class="mission-box pt-50">
					<h2>We’re <span>different</span></h2>
					<p>We’re a true team that collaborates on a minute-to-minute basis within our headquarters just east of San Francisco. We communicate, plan, and execute with unmatched efficiency, skills, and timely delivery of design products that increase your profits.</p>
					<p>We’re not just about logos, or websites, or business cards, or posters. We are about your business.</p>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="front-office">
	<div class="container">
		<h2>the front <span>office</span></h2>
		<div class="row">
			<div class="col-md-5">
				<div class="outer-front-box">
					<div class="inner-full-box">
						<img src="images/img1-person.png">
						<p>With degrees from the Pratt Institute of Design and Wharton School of Business, Charles is that rare combination of pure artist, visionary & effective company leader.</p>
						<h5>Charles Robert Brooks</h5>
						<h6> MFA, MBA - Founder, CEO</h6>
					</div>
				</div>
			</div>
			<div class="col-md-5 col-md-offset-2">
				<div class="outer-front-box">
					<div class="inner-full-box">
						<img src="images/img1-person1.png">
						<p>The Queen of Problem-Solving,” Suzanne oversees all project assignments from point of origin through delivery.</p>
						<h5>Suzanne Gibson</h5>
						<h6>Senior Director of Operations</h6>
					</div>
				</div>
			</div>
		</div>
		<div class="row pt-150">
			<div class="col-md-5">
				<div class="outer-front-box">
					<div class="inner-full-box">
						<img src="images/img1-person2.png">
						<p>Nothing escapes Devon’s review process. All projects are analyzed from a delivery-by-deadline perspective while clients are kept in the communications loop.</p>
						<h5>Devon Williamson</h5>
						<h6>Project Manager</h6>
					</div>
				</div>
			</div>
			<div class="col-md-5 col-md-offset-2">
				<div class="outer-front-box">
					<div class="inner-full-box">
						<img src="images/img1-person3.png">
						<p>A graduate of Los Angeles’ Art Center College of Design, Kevin overseas all design deliverables from concept through production. He has a unique eye for aesthetics as they affect 
consumer behavior.</p>
						<h5>Kevin Martin</h5>
						<h6>MFA - Design Lead</h6>
					</div>
				</div>
			</div>
		</div>
		<div class="row pt-150">
			<div class="col-md-5">
				<div class="outer-front-box">
					<div class="inner-full-box">
						<img src="images/img1-person4.png">
						<p>Shane is responsible for keeping the design team on schedule and on budget. He comes to work early and stays late to make sure clients receive the best possible value on every job. Drinks six cups of coffee before noon.</p>
						<h5>Shane “Rocket” Rogers</h5>
						<h6>Production Manager</h6>
					</div>
				</div>
			</div>
			<div class="col-md-5 col-md-offset-2">
				<div class="outer-front-box">
					<div class="inner-full-box">
						<img src="images/img1-person5.png">
						<p>Lori brings a Harvard Business School appreciation of the bottom line to every client and to every design project. If it doesn’t make sense for the client’s business, Lori works with design team leaders to put it on course.</p>
						<h5>Lori Leigh</h5>
						<h6>MBA - Business Manager</h6>
					</div>
				</div>
			</div>
		</div>
		<div class="row pt-150">
			<div class="col-md-5 col-md-offset-3">
				<div class="outer-front-box">
					<div class="inner-full-box">
						<img src="images/img1-person6.png">
						<p>Elliott is a genuine neuromarketing specialist with an obsessive interest in what motivates end-users and consumers to make a buying decision. The use of design, color, and messaging impacts us all; Elliott brings that knowledge to bear on client projects to increase ROI.</p>
						<h5>Elliott McCumber</h5>
						<h6>MS - Marketing and Consumer Behavior</h6>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="design-office">
	<div class="container">
		<h2>we do <span>Design</span></h2>
		<p>The core design team includes a roster of full-time in-house specialists in graphic logo creation, copywriting, web design and development, print and photo editing and production,
and consumer research. Here are they!</p>
		<img src="images/lola-office.png">
	</div>
</section>
<section class="designprofit-sec">
	<div class="container-fluid">
		<h2>story of <span>designs4profit</span></h2>
		<div class="row">
			<div class="col-md-6 pl-custom pr-tablet">
				<div class="af-tabs">
					<ul class="nav nav-tabs">
					    <li class="active"><a data-toggle="tab" href="#aboutlogo">about logo </a></li>
					    <li><a data-toggle="tab" href="#struggles"> struggles </a></li>
					    <li><a data-toggle="tab" href="#results">results</a></li>
					</ul>
					 <div class="tab-content">
					    <div id="aboutlogo" class="tab-pane fade in active">
					      <p>Company founder Charles Brooks worked his way through business school as a freelance artist (he was already a graduate of Pratt Institute of Design) working remotely for a major online design website, producing hundreds of logos, email templates, and web concepts for clients he never met.</p>
					      <p>Management of that company insulated their sub-contractor designers from the clients out of fear the better designers would approach clients on their own and “poach” the business out from under them. Much of the work Charles was given to do involved reworking the mediocre designs of other freelancers or simply just starting from scratch, which meant that the original artist didn’t get paid.</p>
					    </div>
					    <div id="struggles" class="tab-pane fade">
					      <h3>Lorem Ipsum</h3>
					      <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
					    </div>
					    <div id="results" class="tab-pane fade">
					      <h3>Lorem Ipsum</h3>
					      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
					    </div>
					  </div>
				</div>
			</div>
			<div class="col-md-6 pl-11">
				<div class="mission-img-box1">
					<img src="images/about-logo.png">
				</div>
			</div>
		</div>
	</div>
</section>
<section class="contact-us-sec"> 
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-6">
				<div class="contact-background">
					<div class="inner-contact-box">
						<h2><span>contact</span> us</h2>
						<p>Everyone at <span>Designs4Profit.com</span> enjoys hearing from customers, prospective clients, and anyone who is just “looking around” for the right design team. If you have <span>questions</span>, we have the <span>answers!</span></p>
						<form>
							<div class="row">
								<div class="col-md-6">
									<input type="text" name="Name" placeholder="Name" required="required">
								</div>
								<div class="col-md-6">
									<input type="text" name="email" placeholder="Email" required="required">
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<input type="text" name="phone" placeholder="Phone" required="required">
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<textarea placeholder="Message" required="required"></textarea>
								</div>
							</div>
							<div class="btn-start-box">
								<button>GET STARTED</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="af-map">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2949.0849093813126!2d-71.15537198425216!3d42.340713044315166!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89e37851d37abd8d%3A0x379c741881c340f5!2sChiswick+Rd%2C+Boston%2C+MA+02135%2C+USA!5e0!3m2!1sen!2s!4v1553590086467" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</div>
</section>


<?php include("foot.php") ?>
<?php include("footer.php") ?>