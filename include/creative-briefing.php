<?php include("header.php"); ?>
<?php include("navbar2.php"); ?>

<section class="faq-banner brief">
	<div class="container">
		<h1>Fill out your creative brief to get started?</h1>
	</div>
</section>
<section class="brief-body">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<h2>Need help filling out your creative brief ? </h2>
				<h2>Give us a call at 919.244.2535 anytime M-F, from 9AM to 8PM US Eastern</h2>
			</div>
		</div>
		<div class="row">
			<form class="briefing-form" method="post" autocomplete="off">
				<h3>background <span>information</span></h3>
				<div class="col-md-7">
					<div class="af-brief-form">
						<div class="row">
							<div class="col-sm-6 col-md-6 plr-5 af-gmail-style">
								<input type="text" placeholder="Full Name" name="name" >
								<label>Name</label>
							</div>
							<div class="col-sm-6 col-md-6 plr-5 af-gmail-style">
								<input type="text" placeholder="Your E-mail Address" name="email" > 
								<label>Email</label>
							</div>
							<div class="col-sm-6 col-md-6 plr-5 af-gmail-style">
								<input type="text" placeholder="Your Phone Number" name="phone" >
								<label>Phone</label>
							</div>
							<div class="col-sm-6 col-md-6 plr-5 af-gmail-style">
								<input type="text" placeholder="What name do you want in your logo?" name="logo" >
								<label>Logo</label>
							</div>
							<div class="col-sm-6 col-md-6 plr-5 af-gmail-style">
								<input type="text" placeholder="Logan or tagline in your logo?" name="tagline" >
								<label>Tagline</label>
							</div>
							<div class="col-sm-6 col-md-6 plr-5 af-gmail-style">
								<input type="text" placeholder="Logo Type" name="type" >
								<label>Type</label>
							</div>
							<div class="col-md-12 plr-5 af-gmail-style">
								<textarea name="detail" placeholder="Tell us about your business - What do you do? Who are your customers" ></textarea>
								<label>Detail</label>
							</div>
							<div class="col-md-12 plr-5">
								<div class="row">
									<div class="col-md-7">
										<h4>What industry is your business in?</h4>
									</div>
									<div class="col-md-5">
										<select>
											<option>- Select Industry -</option>
											<option>IT Industry</option>
											<option>Medical Industry</option>
											<option>Advertising Industry</option>
											<option>Insurance Industry</option>
											<option>Media Industry</option>
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-5 af-center">
					<img src="images/brief-illus.png">
				</div>
				<div class="col-md-7 plr-0">
					<h3 class="before">Design <span>Preferences</span></h3>
					<p class="pt-40">Are there any specific colors you'd like us to use in your logo design? Type them in the box below.</p>
				</div>
				<div class="col-md-5">
					<div class="input-fields pt-110">
						<input type="text" name="">
					</div>
				</div>
				<div class="col-md-12 plr-0">
					<h3 class="pb-30">What do different colors represent?</h3>
					<div class="color-represent">
						<div class="af-color af-red"></div>
						<p class="af-inline wd-20">Red</p>
						<p class="af-inline wd-60">Passion, Anger, Love, Confidence</p>
					</div>
					<div class="color-represent">
						<div class="af-color af-orange"></div>
						<p class="af-inline wd-20">Orange</p>
						<p class="af-inline wd-60">Youthfulness, Cheer, Warmth, Hunger</p>
					</div>
					<div class="color-represent">
						<div class="af-color af-yellow"></div>
						<p class="af-inline wd-20">Yellow</p>
						<p class="af-inline wd-60">Sunshine, Happiness, Energy, Optimism</p>
					</div>
					<div class="color-represent">
						<div class="af-color af-green"></div>
						<p class="af-inline wd-20">Green</p>
						<p class="af-inline wd-60">Nature, Fertility, Balance, Cleanliness</p>
					</div>
					<div class="color-represent">
						<div class="af-color af-blue"></div>
						<p class="af-inline wd-20">Blue</p>
						<p class="af-inline wd-60">Water, Tranquility, Trust, Power</p>
					</div>
					<div class="color-represent">
						<div class="af-color af-purple"></div>
						<p class="af-inline wd-20">Purple</p>
						<p class="af-inline wd-60">Nobility, Power, Elegance, Wisdom</p>
					</div>
					<div class="color-represent">
						<div class="af-color af-white"></div>
						<p class="af-inline wd-20">White</p>
						<p class="af-inline wd-60">Peace, Balance, Purity, Simplicity, Winter</p>
					</div>
					<div class="color-represent">
						<div class="af-color af-gray"></div>
						<p class="af-inline wd-20">Gray</p>
						<p class="af-inline wd-60">Neutral, Sophistication, Balance, Wisdom</p>
					</div>
					<div class="color-represent">
						<div class="af-color af-black"></div>
						<p class="af-inline wd-20">Black</p>
						<p class="af-inline wd-60">Exclusivity, Modern, Power, Sophistication, Mystery</p>
					</div>
					<div class="color-represent">
						<div class="af-color af-brown"></div>
						<p class="af-inline wd-20">Brown</p>
						<p class="af-inline wd-60">Earth, Stability, Tradition, Nature</p>
					</div>
					<div class="color-represent">
						<div class="af-color af-pink"></div>
						<p class="af-inline wd-20">Pink</p>
						<p class="af-inline wd-60">Love, Romance, Femininity, Baby Girls, Humanistic</p>
					</div>
					<div class="color-represent">
						<div class="af-color af-turquoise"></div>
						<p class="af-inline wd-20">Turquoise</p>
						<p class="af-inline wd-60">Tranquility, Clarity, Compassion, Healing</p>
					</div>
				</div>
				<div class="col-md-12 plr-0">
					<h3 class="pb-30">Which layout would you like for your logo? (Choose up to 3)</h3>
					<div class="layout-imgs">
						<img src="images/layout-1.png">
						<img src="images/layout-2.png">
						<img src="images/layout-3.png">
						<img src="images/layout-4.png">
						<img src="images/layout-5.png">
						<img src="images/layout-6.png">
						<img src="images/layout-7.png">
						<img src="images/layout-8.png">
					</div>
				</div>
				<div class="col-md-12 plr-0">
					<h3 class="pb-30">How do you plan to use your logo?</h3>
					<label>
						<input type="checkbox" name="">
						Web (Website, banner ads, email marketing)
					</label>
					<label>
						<input type="checkbox" name="">
						Print (Business cards, letterhead, mailers)
					</label>
					<label>
						<input type="checkbox" name="">
						Clothing (T-shirts, hats, embroidery)
					</label>
					<label>
						<input type="checkbox" name="">
						Promotional Items (pens, mugs, USB flash drives)
					</label>
					<label>
						<input type="checkbox" name="">
						Product Packaging/Labels (Boxes, bottles, stickers, bags)
					</label>
					<label>
						<input type="checkbox" name="">
						Large Format (Signs, marquis, billboards, car-wraps)
					</label>
					<label>
						<input type="checkbox" name="">
						<input type="text" name="" placeholder="Something Else">
					</label>
				</div>
				<div class="col-md-7">
					<div class="af-textareas">
						<label>Is there anything you definitely want us <br> to *include* in your logo design?</label>
						<textarea placeholder="I definitely want a dog in my logo"></textarea>
					</div>
					<div class="af-textareas">
						<label>Is there anything you definitely want us to <br> *exclude* from your logo design?</label>
						<textarea placeholder="I definitely don’t want a house or key icon"></textarea>
					</div>
					<div class="af-textareas">
						<label>Is there anything else you'd like to add ?</label>
						<textarea placeholder=""></textarea>
					</div>
				</div>
				<div class="col-md-12">
					<div class="af-choose">
						<h5>Do you have any files, images, sketches, or other <br> documents that might be helpful to our designers?</h5>
						
						<div class="af-choose-file">
							<input id="file" type="file" name="">
							CHOOSE A FILE
						</div>
						<div class="file-name">
							<p>No files attached.</p>
						</div>
					</div>
				</div>
				<div class="col-md-12 text-center">
					<div class="af-subm-btn">
						<input type="submit" name="" value="NEXT">
					</div>
				</div>
			</form>
		</div>
	</div>
</section>

<?php include("foot.php") ?>
<?php include("footer.php") ?>