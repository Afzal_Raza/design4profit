<?php include("header.php"); ?>
<?php include("navbar2.php"); ?>

<section class="client-testimonials">
<div class="main">
  <div class="container container--wide">
  	<h2>Our clients say the <span>nicest things!</span></h2>
  	<div class="col-md-12 col-lg-10 col-lg-offset-1">
	    <div class="image-grid are-images-unloaded" data-js="image-grid">
	      <div class="image-grid__col-sizer"></div>
	      <div class="image-grid__gutter-sizer"></div>
	        
	        <div class="image-grid__item color-lemon">
	          <h6>Unbelievably friendly service. I’ve never had such a good online experience! The logo I have now sets the tone for the entire restaurant.</h6>
	          <p>Jimmy Shimakawa</p>
	          <p>Daikokuya Little Tokyo</p>
	          <p>Los Angeles</p>
	        </div>
	        <div class="image-grid__item color-aqua">
	          <h6>Our old website needed help. We got far more than we expected.</h6>
	          <p>Martin Prey</p>
	          <p>Museum of Modern Art</p>
	          <p>Los Angeles</p>
	        </div>
	        <div class="image-grid__item color-lavendar">
	          <h6>We started with a logo...and that led to a valuable business relationship. The team at Designs4Profit.com know what they’re doing.</h6>
	          <p>Katherine Garcia</p>
	          <p>Santa Barbara Bank & Trust</p>
	          <p>Santa Barbara, Ca</p>
	        </div>
	        <div class="image-grid__item color-beige">
	          <h6>We had our own logo idea, but the artists at Designs4Profit.com showed us something far better. Thank you!</h6>
	          <p>David Fielding</p>
	          <p> Borders’ Books</p>
	          <p>Fifth Avenue, NYC</p>
	        </div>
	        <div class="image-grid__item color-pinkish">
	          <h6>Very easy to work with. Will be back for more.</h6>
	          <p>Larry Title - Title</p>
	          <p>Schlossberg & Snapp PLC</p>
	          <p>Los Angeles</p>
	        </div>
	        <div class="image-grid__item color-alice">
	          <h6>We trusted them with redesigning our entire marketing look. They were fast and efficient...and right about everything. Business is much better today!</h6>
	          <p>Marie Entwiler</p>
	          <p>Entwiler Consultants LLC</p>
	          <p>San Mateo, Ca</p>
	        </div>
	    </div>

	    <div class="scroller-status">
	      <div class="loader-ellips infinite-scroll-request">
	        <span class="loader-ellips__dot"></span>
	        <span class="loader-ellips__dot"></span>
	        <span class="loader-ellips__dot"></span>
	        <span class="loader-ellips__dot"></span>
	      </div>
	      <p class="scroller-status__message infinite-scroll-last"></p>
	      <p class="scroller-status__message infinite-scroll-error"></p>
	    </div>

	      <p class="pagination">
	        <a class="pagination__next" href="testimonial2"></a>
	      </p>
  	</div>
    <footer class="full-page-demo-footer"></footer>

  </div>

</div>
</section> 


<?php include("foot.php") ?>
<?php include("footer.php") ?>