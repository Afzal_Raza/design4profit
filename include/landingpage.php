<?php include("header.php"); ?>
<?php include("navbar.php"); ?>

<section class="home-banner">
	<div class="container">
		<div class="row">
			<div class="col-xs-8 col-sm-7 col-md-7">
				<h1 class="cd-headline clip is-full-width yui3-cssreset">Behind every <span>Great</span><br><span>Business...</span>IS a <span class="cd-words-wrapper"> <b class="is-visible">Great Design</b>
				<b>Great Idea</b>
				<b>Great Team</b></span></h1>
				<a href="javascript:void(0);">Get <span>Started</span></a>
			</div>
			<div class="col-xs-4 col-sm-5 col-md-5">
				<div class="banner-img-box">
					<img class="banner-img" src="images/banner-laptop.png">
				</div>
			</div>
			<!-- <div class="banner-half">
				<h1 class="cd-headline clip is-full-width">Behind every <span>Great</span><br><span>Business...</span>IS a <span class="cd-words-wrapper"> <b class="is-visible">Great Design</b>
				<b>Great Idea</b>
				<b>Great Team</b></span></h1>
				<a href="javascript:void(0);">Get <span>Started</span></a>
			</div> -->
			<!-- <div class="banner-carousel">
				<div class="owl-carousel">
			  		<div class="banner-img-box">
						<img class="banner-img" src="images/banner-laptop.png">
					</div>
					<div class="banner-img-box">
						<img class="banner-img" src="images/banner-laptop.png">
					</div>
					<div class="banner-img-box">
						<img class="banner-img" src="images/banner-laptop.png">
					</div>
					<div class="banner-img-box">
						<img class="banner-img" src="images/banner-laptop.png">
					</div>
				</div>	
			</div> -->
		</div>
	</div>
</section>
<section class="graphic-design-sec">
	<div class="container">
		<div class="row pb-150">
			<div class="col-xs-12 col-sm-12 col-md-6">
				<div class="graphic-content">
					<h2><span>Graphics</span> and <span>logos</span><br> that makes you more <span>money</span></h2>
					<p>You’re in business to make money.<br>We help you do that with innovative, progressive designs, logos, and graphic messaging.</p>
					<p>Our team is more than just “creative.” </p>
					<p>Whether it’s your new logo, an entire website design, or graphic branding, Designs4Profit.com is all about building <br> your business.</p>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6">
				<div class="graphic-img-box">
					<span></span>
					<span></span>
					<span></span>
					<span></span>
					<img src="images/graphic-img.png">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-6">
				<div class="design-img-box">
					<span></span>
					<span></span>
					<span></span>
					<span></span>
					<img src="images/design-img.png">
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6">
				<div class="graphic-content">
					<h2>we don’t just <span>talk design</span> <br> we do <span>Design</span></h2>
					<p>When we work with you, things move quickly. It’s an accelerated process at Designs4Profit.com: everything we do is about solving your business challenges starting on Day One.</p>
					<p>Tell us your design preferences and we go to work. Within days you’ll have a choice of solutions that will put your competition to shame.</p>
					<div class="row pt-5">
						<div class="col-xs-4 col-sm-4 col-md-4">
							<div class="company-box">
								<img src="images/apple-logo.png">
								<p>The Apple logo is a branding icon.</p>
							</div>
						</div>
						<div class="col-xs-4 col-sm-4 col-md-4">
							<div class="company-box">
								<img src="images/nike-logo.png">
								<p>The Nike logo is a guarantee of quality.</p>
							</div>
						</div>
						<div class="col-xs-4 col-sm-4 col-md-4">
							<div class="company-box">
								<img src="images/amazon-logo.png">
								<p>The Amazon logo is a promise of efficiency.</p>
							</div>
						</div>
					</div>
					<p>That’s what Designs4Profit.com is all about</p>
					<p>We don’t waste your time with excuses.<br> We simply deliver the results you need to your company’s bottom line.</p>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="business-part-sec">
	<div class="container">
		<h2>when it comes to your <span>business</span> <br> <span>design4profit.com</span> is your <span>Design partner !</span></h2>
		<p>We deliver everything from progressive</p>
		<div class="row">
			<div class="col-xs-6 col-sm-6 col-md-4">
				<div class="boxes-business">
					<div class="design-pen-box">
						<span></span>
						<span></span>
						<span></span>
						<span></span>
						<span></span>
						<img src="images/pen-background.png">
						<img class="upper-img" src="images/pen.png">
					</div>
					<h3>Cutting Edge Logo Design</h3>
				</div>
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4">
				<div class="boxes-business">
					<div class="media-secreen-box">
						<span></span>
						<span></span>
						<span></span>
						<span></span>
						<img src="images/web-design-img.png">
						<img class="screen-img" src="images/WebDesign.png">
					</div>
					<h3>Web Design</h3>
				</div>
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4 col-xs-offset-3 col-sm-offset-3 col-md-offset-0">
				<div class="boxes-business">
					<div class="business-full-box">
						<span></span>
						<span></span>
						<span></span>
						<span></span>
						<span></span>
						<img src="images/business-back.png">
						<img class="business-img" src="images/Businesscards.png">
					</div>	
					<h3>Business Cards</h3>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-6 col-sm-6 col-md-4 col-md-offset-2">
				<div class="boxes-business">
					<div class="speaker-img-box">
						<span></span>
						<span></span>
						<span></span>
						<img src="images/social-media-back.png">
						<img class="media-imge" src="images/social-media-imgs.png">
					</div>
					<h3>Social Media Pages</h3>
				</div>
			</div>
			<div class="col-xs-6 col-sm-6 col-md-4">
				<div class="boxes-business">
					<div class="corporate-inner">
						<span></span>
						<span></span>
						<span></span>
						<span></span>
						<span></span>
						<img src="images/branding-imgs.png">
						<img class="corparete-img" src="images/corp-image.png">
					</div>
					<h3>Corporate Branding</h3>
				</div>
			</div>
		</div>
		<div class="ask-inline">
			<p>Ask Us questions, and let our Design Team show you the answers.</p>
			<a href="#">Contact Us</a>
		</div>
	</div>
</section>
<section class="business-story-sec">
	<div class="container">
		<h2>We deliver more than <span>design</span></h2>
		<p> We deliver results with a seamless process</p>
		<div class="row pt-70">
			<div class="col-md-8">
				<div class="business-content wow fadeInLeft">
					<div class="col-xs-7 col-sm-7 col-md-7">
						<img src="images/deliver-img1.png">
					</div>
					<div class="col-xs-5 col-sm-5 col-md-5">
					<h6 class="mt-90">01</h6>
						<p>Tell us what your design needs are. Killer logo? Dynamic website? Compelling posters, business cards, newsletters, or emails? We do it all...and more.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row pt-40">
			<div class="col-md-9 col-md-offset-3">
				<div class="business-content wow fadeInRight">
					<div class="col-xs-7 col-sm-7 col-md-7">
						<div class="img-box">
							<img src="images/deliver-img2.png">
						</div>
					</div>
					<div class="col-xs-5 col-sm-5 col-md-5">
					<h6 class="mt-60">02</h6>
						<p>Tell us about your business and your short- and long-range objectives. If you have design requirements (color-theme-style), let us know. We’re happy to provide a personal phone consultation to share ideas.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row pt-40">
			<div class="col-md-8">
				<div class="business-content wow fadeInLeft">
					<div class="col-xs-7 col-sm-7 col-md-7 text-center">
						<img src="images/deliver-img3.png">
					</div>
					<div class="col-xs-5 col-sm-5 col-md-5">
						<h6 class="mt-80">03</h6>
						<p>Select one of our value-driven design packages. They’re the most cost-effective solution to consistent branding for all your needs, including original logo design, business cards & brochures, and website design, navigation, and functionality.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row pt-40">
			<div class="col-md-9 col-md-offset-3">
				<div class="business-content wow fadeInRight">
					<div class="col-xs-7 col-sm-7 col-md-7">
						<img src="images/deliver-img4.png">
					</div>
					<div class="col-xs-5 col-sm-5 col-md-5">
						<h6 class="mt-10">04</h6>
						<p>Get ready to be blown away. In just a few days we’ll send you a selection of innovative, completely original design concepts focused on a single objective: bringing you more business and greater profits. Your feedback will be important, as all final design decisions are yours.  A collaborative relationship between you and our Design Team always delivers the best results.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="goto-work-box">
			<a href="javascript:void(0)">Let’s go to work!</a>
		</div>
	</div>
</section>
<section class="client-testimonials home">
	<div class="container">
		<h2>Our clients say the <span>nicest things!</span></h2>
		<div class="col-md-12 col-lg-10 col-lg-offset-1">
			<div class="full-testi-box">
				<div class="testimonial-box color-purple notes-style-box1">
					<h6>You guys are the real deal. No redundant “clip art” here. My new business logo has already brought new business.</h6>
					<p>Dave M., CMO Allied </p>
					<p>Enterprises, Los Angeles, Ca.</p>
				</div>
				<div class="testimonial-box color-blue notes-style-box2">
					<h6>We’ve used the same website design for almost ten years and our revenue growth was stalled. Your design now has us at the top of the search engines, and new customers are streaming in. I’m just glad we found you when we did.</h6>
					<p>H. Steinbaum  </p>
					<p>President</p>
					<p>WhereHollywoodHides.com</p>
				</div>
				<div class="testimonial-box color-skin notes-style-box3">
					<h6>It’s hard to believe that a new logo could increase our quarterly revenues, but there is no other explanation. You did exactly what you said you would...you brought more customers to our site!</h6>
					<p>Hillary M. </p>
					<p>CFO surfshirts.com</p>
				</div>
				<div class="testimonial-box color-green notes-style-box4">
					<h6>Your design team was very helpful and easy to work with...even though we wanted lots of adjustments to our printed sales materials. The final product made our competitors envious.</h6>
					<p>Sean O.</p>
					<p>Field Sales Mgr.</p>
					<p>The Bardash Company</p>
				</div>
				<div class="testimonial-box color-pink notes-style-box5">
					<h6>We’ve been disappointed (screwed, actually) by other so-called design companies. But you did exactly what we agreed to, and you delivered early. Amazing service, great design. Thanks again.</h6>
					<p>Wanda S. </p>
					<p>Senior Director of Marketing,</p>
					<p>FDSI Enterprises</p>
				</div>
				<div class="testimonial-box color-sky notes-style-box6">
					<h6>Wow! The business card you delivered looks like a collector’s item. Everyone i hand it to asks for another to show their boss. I have a feeling that’s the best recommendation ever.</h6>
					<p>Alphonso H.</p>
					<p>Media Mgr. Hilton Head Island </p>
				</div>
			</div>
				<div class="goto-work-box">
					<a href="testimonial">LOAD MORE</a>
				</div>
		</div>
	</div>
</section>
<section class="how-work-sec">
	<div class="container">
		<h2>how we <span>work</span></h2>
		<div class="row">
			<div class="col-sm-4 col-md-4 pb-30">
				<div class="work-boxes">
					<img src="images/pen-img.png">
					<h2>Designing</h2>
					<p>The majority of “online design” companies don’t actually do any design work.<br>They farm it out to freelance sub-contractors through other websites that conduct bid-design “contests” which pit amateur graphic design wannabes against each other. </p>
					<p>The end-customer chooses the design they like and that designer gets paid a percentage of the fee...often a very small percentage.</p>
					<p>The other designers who worked in the hope get winning the assignment get nothing.</p>
				</div>
			</div>
			<div class="col-sm-4 col-md-4 pb-30">
				<div class="work-boxes">
					<img src="images/star-img.png">
					<h2>Values</h2>
					<p>Two things are wrong with this system of “spec work”:</p>
					<ol>
						<li>It’s unethical as it exploits the work of truly skilled designers who may or may not (very often) get paid for their work.</li>
						<li>It guarantees a rush job of poor design quality.</li>
					</ol>
					<p>Our design team doesn’t work that way. We have a loyal, dedicated team of designers because we pay them well during the design process all the way through delivery to the client and follow up support.</p>
				</div>
			</div>
			<div class="col-sm-4 col-md-4">
				<div class="work-boxes">
					<img src="images/weight-img.png">
					<h2>Ethics</h2>
					<p>Everyone at Designs4Profit.com becomes part of your team, dedicated to delivering high value and unparalleled design quality for your project. It doesn’t matter if your project is large or small; we share your business goals work hard to fulfill them with the most professional design work you’ll find anywhere.</p>
					<p>The response from our clients has been remarkable. That’s because we deliver remarkable solutions and service...and because we do it ethically, honestly, and fairly.</p>
					<p>Our success depends on it.</p>
				</div>
			</div>
		</div>
		<div class="goto-work-box">
			<a href="javascript:void(0)">Find your Value Design Package</a>
		</div>
	</div>
</section>


<?php include("foot.php") ?>
<?php include("footer.php") ?>