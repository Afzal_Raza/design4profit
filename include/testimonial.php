<?php include("header.php"); ?>
<?php include("navbar2.php"); ?>

<section class="client-testimonials">
<div class="main">
  <div class="container container--wide">
  	<h2>Our clients say the <span>nicest things!</span></h2>
  	<div class="col-md-12 col-lg-10 col-lg-offset-1">
	    <div class="image-grid are-images-unloaded" data-js="image-grid">
	      <div class="image-grid__col-sizer"></div>
	      <div class="image-grid__gutter-sizer"></div>
	        
	        <div class="image-grid__item color-lemon">
	          <h6>Unbelievably friendly service. I’ve never had such a good online experience! The logo I have now sets the tone for the entire restaurant.</h6>
	          <p>Jimmy Shimakawa</p>
	          <p>Daikokuya Little Tokyo</p>
	          <p>Los Angeles</p>
	        </div>
	        <div class="image-grid__item color-aqua">
	          <h6>Our old website needed help. We got far more than we expected.</h6>
	          <p>Martin Prey</p>
	          <p>Museum of Modern Art</p>
	          <p>Los Angeles</p>
	        </div>
	        <div class="image-grid__item color-lavendar">
	          <h6>We started with a logo...and that led to a valuable business relationship. The team at Designs4Profit.com know what they’re doing.</h6>
	          <p>Katherine Garcia</p>
	          <p>Santa Barbara Bank & Trust</p>
	          <p>Santa Barbara, Ca</p>
	        </div>
	        <div class="image-grid__item color-beige">
	          <h6>We had our own logo idea, but the artists at Designs4Profit.com showed us something far better. Thank you!</h6>
	          <p>David Fielding</p>
	          <p> Borders’ Books</p>
	          <p>Fifth Avenue, NYC</p>
	        </div>
	        <div class="image-grid__item color-pinkish">
	          <h6>Very easy to work with. Will be back for more.</h6>
	          <p>Larry Title - Title</p>
	          <p>Schlossberg & Snapp PLC</p>
	          <p>Los Angeles</p>
	        </div>
	        <div class="image-grid__item color-alice">
	          <h6>We trusted them with redesigning our entire marketing look. They were fast and efficient...and right about everything. Business is much better today!</h6>
	          <p>Marie Entwiler</p>
	          <p>Entwiler Consultants LLC</p>
	          <p>San Mateo, Ca</p>
	        </div>
	        <div class="image-grid__item color-magenta">
	          <h6>Our founder created our first logo. Not good. Convinced him to try Designs4Profit.com. Now we look like professionals.</h6>
	          <p>George Goldenberg</p>
	          <p>Texas Oil & Exploration</p>
	          <p>Dallas Tx</p>
	        </div>
	        <div class="image-grid__item color-deepsky">
	          <h6>We needed a working website fast. We got it...and it sells our stuff like crazy. Thanks to you guys for everything!</h6>
	          <p>Tiffany Shoemaker</p>
	          <p>Tiffany.com</p>
	        </div>
	        <div class="image-grid__item color-wheat">
	          <h6>The stationery business can be pretty boring. Designs4Profit changed that and our business increased by 20% the first month.</h6>
	          <p>Margo Freeman</p>
	          <p>The Paper Mill</p>
	          <p>San Francisco, Ca.</p>
	        </div>	
          <div class="image-grid__item color-seashell">
            <h6>The company marketing materials we were using were stale. Thanks for the refresh.</h6>
            <p>John Quincy</p>
            <p>Metals Management Systems</p>
            <p>Phoenix, AZ</p>
          </div>
          <div class="image-grid__item color-mint">
            <h6>They were the high bid, but we liked what we saw. At the end of the day, their high bid was a bargain.</h6>
            <p>Monty Monan</p>
            <p>Florida Marlin Association</p>
            <p>Miami, FL</p>
        </div>
        
        <div class="image-grid__item color-dew">
            <h6>We’re in the customer service business too, but the team at Design4Profits.com showed us how it’s done properly. Very impressive folks. Excellent designs too!</h6>
            <p>Linda Laisa</p>
            <p>Saks Fifth Avenue, NYC</p>
        </div>
        <div class="image-grid__item color-hotpink">
            <h6>We ordered the Premier Design Package. It was top-to-bottom awesome.</h6>
            <p>Ronnie Gardner</p>
            <p>National Hot Rod Association</p>
            <p>Chicago, IL</p>
        </div>
        <div class="image-grid__item color-indigo">
          <h6>They weren’t the cheapest we could find, but after two horrible experiences, it was a relief to deal with real professionals.</h6>
          <p>Dora Ryan</p>
          <p>International Marketplace</p>
          <p>Seattle, WA</p>
        </div>
        <div class="image-grid__item color-mistyrose">
           <h6>Enjoyable process, great people, wonderful work. Highly recommended.</h6>
           <p>Tonya Lasser</p>
           <p>Income Tax Associates</p>
           <p>Philadelphia, PA</p>
        </div>
        <div class="image-grid__item color-gold">
            <h6>Designs4Profits.com made our business look exciting. Increased sales by 15% in the first month. Whoa.</h6>
            <p>Nathan Coombs</p>
            <p>Charleston Automotive</p>
            <p> Charleston, VA</p>
        </div>
        <div class="image-grid__item color-slate">
          <h6>Our personal design project manager handled it all. Could not have been easier and the result was more than we hoped for.</h6>
          <p>Wayne Miller</p>
          <p>San Diego Surf Company</p>
        </div>
        <div class="image-grid__item color-seagreen">
          <h6>Everybody we talked to was responsive and friendly. The designs required several revisions, but the end result was worth it.</h6>
          <p>Eric Timpanis</p>
          <p>Marketing Director</p>
          <p>Global Travel Services</p>
        </div>
        <div class="image-grid__item color-slategrey">
          <h6>Our business jumped significantly when we promoted our new logo. So glad we did this.</h6>
          <p>Mike Jarvis</p>
          <p>American Steakhouse Restaurants</p>
          <p>St. Louis, MO</p>
        </div>
        <div class="image-grid__item color-antique">
          <h6>Efficient, fast, friendly. With a great product. Can’t ask for more. Thanks.</h6>
          <p>Marsha Dorner</p>
          <p>Western Fashion Center</p>
          <p> Los Angeles, Ca</p>
        </div>

          <div class="image-grid__item color-purple">
              <h6>You guys are the real deal. No redundant “clip art” here. My new business logo has already brought new business.</h6>
              <p>Dave M., CMO Allied </p>
              <p>Enterprises, Los Angeles, Ca.</p>
          </div>
          <div class="image-grid__item color-blue">
             <h6>We’ve used the same website design for almost ten years and our revenue growth was stalled. Your design now has us at the top of the search engines, and new customers are streaming in. I’m just glad we found you when we did.</h6>
              <p>H. Steinbaum  </p>
              <p>President</p>
              <p>WhereHollywoodHides.com</p>
          </div>
          <div class="image-grid__item color-skin">
            <h6>It’s hard to believe that a new logo could increase our quarterly revenues, but there is no other explanation. You did exactly what you said you would...you brought more customers to our site!</h6>
            <p>Hillary M. </p>
            <p>CFO surfshirts.com</p>
          </div>
          <div class="image-grid__item color-green">
            <h6>Your design team was very helpful and easy to work with...even though we wanted lots of adjustments to our printed sales materials. The final product made our competitors envious.</h6>
            <p>Sean O.</p>
            <p>Field Sales Mgr.</p>
            <p>The Bardash Company</p>
          </div>
          <div class="image-grid__item color-pink">
            <h6>We’ve been disappointed (screwed, actually) by other so-called design companies. But you did exactly what we agreed to, and you delivered early. Amazing service, great design. Thanks again.
            </h6>
            <p>Wanda S. </p>
            <p>Senior Director of Marketing,</p>
            <p>FDSI Enterprises</p>
          </div>
          <div class="image-grid__item color-sky">
            <h6>Wow! The business card you delivered looks like a collector’s item. Everyone i hand it to asks for another to show their boss. I have a feeling that’s the best recommendation ever.</h6>
            <p>Alphonso H.</p>
            <p>Media Mgr. Hilton Head Island </p>
          </div>
	    </div>
  	</div>
  </div>
</div>
</section> 


<?php include("foot.php") ?>
<?php include("footer.php") ?>